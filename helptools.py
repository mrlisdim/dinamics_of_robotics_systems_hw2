# -*- coding: utf-8 -*-
"""
Created on Tue Nov  1 18:12:24 2016

@author: mrlisdim
"""

import sympy as sp
from sympy import cos as Cos
from sympy import sin as Sin
from sympy import Matrix
from sympy.physics.mechanics import mlatex
  
  
def get_transform_matrix(d, theta, r, alpha):
    return get_z_transform_matrix(d, theta) * \
           get_x_transform_matrix(r, alpha)
   
    
def get_z_transform_matrix(d, theta):
    Z = Matrix([
              [Cos(theta), -Sin(theta), 0, 0],
              [Sin(theta),  Cos(theta), 0, 0],
              [0,           0,          1, d],
              [0,           0,          0, 1]
              ])
    
    return Z
    
    
def get_x_transform_matrix(r, alpha):
    X = Matrix([
              [1,  0,           0,          r],
              [0,  Cos(alpha), -Sin(alpha), 0],
              [0,  Sin(alpha),  Cos(alpha), 0],
              [0,  0,           0,          1]
              ])
    
    return X

    
def get_rotation_from_transform_matrix(T):
    return T[0:3, 0:3]
    
    
def get_translation_from_transform_matrix(T):
    return T[0:3, 3]
    

def diff_list(l):
    return list(map(lambda x: sp.diff(x), l))
    
    
def prMlat(expr):
    print(mlatex(expr))