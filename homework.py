# -*- coding: utf-8 -*-
"""
@author: mrlisdim
"""

import sympy as sym
from sympy import Matrix
import sympy.physics.mechanics as mec

import helptools as ht
import ne

mec.init_vprinting()

n = 2

m = sym.symbols('m1:{0}'.format(n+1))

r = sym.symbols('r1:{0}'.format(n+1))
alpha = sym.symbols('alpha1:{0}'.format(n+1))
d = sym.symbols('d1:{0}'.format(n+1))
theta = sym.symbols('theta1:{0}'.format(n+1))

h = sym.Symbol('h')
l = sym.Symbol('l')

Izz = sym.symbols('Izz1:{0}'.format(n+1))
Ixx = sym.symbols('Ixx1:{0}'.format(n+1))
Iyy = sym.symbols('Iyy1:{0}'.format(n+1))

q = mec.dynamicsymbols('q')
x = mec.dynamicsymbols('x')

g = sym.Symbol('g')

dh_params = {'d':    [x,         0],
             'theta':[-sym.pi/2, q],
             'r':    [h/2,       l],
             'alpha':[-sym.pi/2, 0]}

dh_subst = dict(zip(r, dh_params['r']))
dh_subst = {**dh_subst, **dict(zip(alpha, dh_params['alpha']))}
dh_subst = {**dh_subst, **dict(zip(theta, dh_params['theta']))}
dh_subst = {**dh_subst, **dict(zip(d,     dh_params['d']))}

T = [ht.get_transform_matrix(d[i], theta[i], r[i], alpha[i]).subs(dh_subst) \
     for i in range(n)]

R = [Matrix([[0, 0, 0], [0, 0, 0], [0, 0, 0]])]
R.extend([ht.get_rotation_from_transform_matrix(T[i]) for i in range(n)])

r_e = [Matrix([[0], [0], [0]])]
for i in range(1,n+1):
    r_ei = R[i].transpose() * ht.get_translation_from_transform_matrix(T[i-1])
    r_ei = sym.simplify(r_ei)
    r_e.append(r_ei)

center_mass_shift = [Matrix([[0], [0], [0]])] * (n+1)
center_mass_shift[1] = Matrix([[-h/2], [0], [0]])
center_mass_shift[2] = Matrix([[0], [0], [0]])

r_c = [r_e[i] + center_mass_shift[i] for i in range(n+1)];

I = [Matrix([[Ixx[i],0,0],
             [0,Iyy[i],0],
             [0,0,Izz[i]]
             ]) for i in range(n)]

z = [Matrix([[0],[0],[1]])] * (n + 1)
for i in range(1,n+1):
    z[i] = R[i].transpose() * z[i-1]


omega_init = Matrix([[0],[0],[0]])
omega_d_init = Matrix([[0],[0],[0]])
acc_init = Matrix([[0],[g],[0]])

omega = [omega_init] * (n + 1)
omega_d = [omega_d_init] * (n + 1)
acc = [acc_init] * (n + 1)
acc_c = [acc_init] * (n + 1)

joint_is_prismatic = [None, True, False]
common_coords = [None, x, q]

for i in range(1, n+1):
    omega[i] = ne.get_omega(R[i], omega[i-1], 
                            common_coords[i], z[0],
                            joint_is_prismatic[i])
    
    # R, omega_prev, omega_d_prev, q, z0
    omega_d[i] = ne.get_omega_d(R[i], omega[i-1], omega_d[i-1],
                                common_coords[i], z[0],
                                joint_is_prismatic[i])

    # R, acc_prev, q, z0, omega, omega_d, omega_prev, omega_d_prev, r
    acc[i] = ne.get_acc(R[i], acc[i-1], common_coords[i],
                        z[0], omega[i], omega_d[i],
                        omega[i-1], omega_d[i-1], r_e[i],
                        joint_is_prismatic[i])
    
    # acc, omega, omega_d, r_c
    acc_c[i] = ne.get_acc_c(acc[i], omega[i], omega_d[i], r_c[i])


f = [Matrix([[0],[0],[0]])] * (n+2)
mu = [Matrix([[0],[0],[0]])] * (n+2)
tau = [Matrix([[0],[0],[0]])] * (n+1)

mass = [0]
mass.extend(m)

iner = [0]
iner.extend(I)

for i in range(n,0,-1):
    rot_matr = None
    if i+1 != n+1:
        rot_matr = R[i + 1]
        
    # f_next, m, acc_c, R    
    f[i] = ne.get_f(f[i+1], mass[i], acc_c[i], rot_matr)
    f[i] = sym.simplify(f[i])
    
    # mu_next, f_next, f, r, r_c, I, m, acc_c, omega_d, omega, R
    mu[i] = ne.get_mu(mu[i+1], f[i+1], f[i], r_e[i], r_c[i], iner[i], mass[i], 
                      acc_c[i], omega_d[i], omega[i], rot_matr)
    mu[i] = sym.simplify(mu[i])
    
    tau[i] = ne.get_tau(f[i], mu[i], R[i], z[0], joint_is_prismatic[i])
    tau[i] = sym.simplify(tau[i])

tau = [sym.expand(elem[0]) for elem in tau[1:]]


common_coords = common_coords[1:]
common_coords_d = ht.diff_list(common_coords);
common_coords_dd = ht.diff_list(common_coords_d);

gravitation_matrix = ne.get_gravitation_matrix(tau, g)
coriolis_matrix = ne.get_coriolis_matrix(tau, common_coords)
inertia_matrix = ne.get_inertia_matrix(tau, common_coords)

