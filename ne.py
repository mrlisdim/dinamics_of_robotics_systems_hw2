# -*- coding: utf-8 -*-
"""
@author: mrlisdim
"""

import sympy as sp
from sympy import Matrix
from sympy import diff as D
from helptools import diff_list


def DD(q):
    return D(D(q))
    
    
def cross2(lhs, rhs):
    return lhs.cross(lhs.cross(rhs))


def get_omega(R, omega_prev, q, z0, prizmatic=True):
    sigma = 0
    if prizmatic:
        sigma = 1

    RT = R.transpose()
    z = RT * z0
    
    omega = RT * (omega_prev + (1 - sigma) * D(q) * z)
    
    return omega


def get_omega_d(R, omega_prev, omega_d_prev, q, z0, prizmatic=True):
    sigma = 0
    if prizmatic:
        sigma = 1
        
    RT = R.transpose()
    z = RT * z0
    
    omega_common = RT * omega_d_prev;
    omega_rot = (RT * omega_prev).cross(D(q) * z) + DD(q) * z
    
    omega = omega_common + (1 - sigma) * omega_rot
    
    return omega
 

def get_acc(R, acc_prev, q, z0, omega, omega_d, omega_prev, omega_d_prev, r, prizmatic=True):
    sigma = 0
    if prizmatic:
        sigma = 1

    RT = R.transpose()
    z = RT * z0

    acc_common = RT * (omega_d_prev.cross(r) +\
                       cross2(omega_prev, r) +\
                       acc_prev)

    acc_lin = 2 * omega.cross(D(q) * z) + DD(q) * z
    
    acc = acc_common + sigma * acc_lin
    
    return acc


def get_acc_c(acc, omega, omega_d, r_c):
    acc_c = omega_d.cross(r_c) + cross2(omega, r_c) + acc
    
    return acc_c


def get_f(f_next, m, acc_c, R=None):
    F = m * acc_c
    f = F

    if R is not None:
        f += R * f_next
    
    return f
    
    
def get_mu(mu_next, f_next, f, r, r_c, I, m, acc_c, omega_d, omega, R=None):
    F = m * acc_c
    N = I * omega_d + omega.cross(I * omega)
    mu = N + r_c.cross(F)
    
    if R is not None:
        mu += R * mu_next + r.cross(R * f_next)
    
    return mu
    

def get_tau(f, mu, R, z0, prizmatic=True):
    RT = R.transpose()
    z = RT * z0
    
    tau = None
    if prizmatic:
        tau = f.transpose() * z
    else:
        tau = mu.transpose() * z
    
    return tau

    
def get_gravitation_matrix(tau, g):
    gr = [[elem.coeff(g)] for elem in tau]
    
    return Matrix(gr)
    
    
def get_coriolis_matrix(tau, common_coords):
    n = len(common_coords)
    c_d = diff_list(common_coords)
    
    coriolis_matrix_rows = []
    for i in range(n):
        coriolis_matrix_rows.append(
            [tau[i].coeff(c_d[j]**2)*c_d[j] for j in range(n)]                                        
        )
    
    return Matrix(coriolis_matrix_rows)
    

def get_inertia_matrix(tau, common_coords):
    n = len(common_coords)
    c_dd = diff_list(diff_list(common_coords))
 
    inertia_matrix_rows = []
    for i in range(n):
        inertia_matrix_rows.append(
            [tau[i].coeff(c_dd[j]) for j in range(n)]                                        
        )
    
    return Matrix(inertia_matrix_rows)
    