# -*- coding: utf-8 -*-
"""
Created on Tue Nov  1 18:12:24 2016

@author: mrlisdim
"""
import sympy as sym

params = {'d':    [0,         0],
          'r':    [0,         0],
          'alpha':[-sym.pi/2, sym.pi/2],
          'theta':[-sym.pi/2, 0]}